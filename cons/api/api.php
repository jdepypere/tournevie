<?php
/**
 * Call session_start before any other output
 **/
session_start();

/**
 * Require the slim framework
 **/
require_once(__DIR__ . "/Slim/Slim.php");
require_once(__DIR__ . "/pdoconnect.php");

require_once(__DIR__ . "/services/ItemService.php");
require_once(__DIR__ . "/services/FinancesService.php");
require_once(__DIR__ . "/services/TableService.php");
require_once(__DIR__ . "/services/TableEnum.php");

/**
 * Create Slim and set settings
 **/
\Slim\Slim::registerAutoLoader();
$app = new \Slim\Slim();
$app->response->headers->set('Content-Type', 'application/json');
$app->response->headers->set('Access-Control-Allow-Origin', 'http://info.tournevie.be');
$app->response->headers->set('Access-Control-Allow-Origin', 'http://sales.tournevie.be');
$app->response->headers->set('Access-Control-Allow-Methods', 'GET');
$app->error(function() use ($app) {
    if (isset($GLOBALS["error"])) {
        echo json_encode(["errorMessage" => $GLOBALS["error"]]);
    } else {
        echo json_encode(["errorMessage" => "Er is een algemene fout opgetreden."]);
    }
});

/**
 * Hook (called before every dispatch of the event)
 **/
$app->hook("slim.before.dispatch", function() use ($app) {
    if ($app->request->getResourceUri() != '/items' && !isset($_SESSION["login"])) {
        /**
         * If a user is not logged in, stop here! Otherwise all functions below need a check
         * unless the call is to /items, since this is used for a the public as well
         **/
        $app->response->setStatus(401);
        $GLOBALS["error"] = "Je bent niet aangemeld.";
        $app->error();
    }

    if ($app->request->isPost()) {
        /**
         * Parse body of POST requests to php vars
         **/
        $GLOBALS["data"] = json_decode($app->request->getBody());
    }
});

$app->group('/items', function() use ($app) {

    /**
     * GET naar /api/items
     * Dit geeft een lijst van alle items terug
     **/
    $app->get('/', function() {
        global $DBH;
        $STH = $DBH->query("SELECT * FROM " . TableService::getTable(TableEnum::ITEMS) . " ORDER BY Category");
        echo json_encode($STH->fetchAll());
    });

    /**
     * GET naar /api/items/categories
     * Dit geeft een lijst van alle items terug
     **/
    $app->get('/categories', function() {
        global $DBH;
        $STH = $DBH->query("SELECT DISTINCT Category FROM " . TableService::getTable(TableEnum::ITEMS));
        echo json_encode($STH->fetchAll());
    });

	 /**
     * GET naar /api/items/:itemId
     * Get value of one item
     **/
    $app->get('/:id', function($id) {
        generateResponse(ItemService::getItemById($id));
    });

    /**
     * POST naar /api/items
     * POST, dus data van client naar server. We gaan een nieuw item opslaan
     **/
    $app->post('/', function() use ($app) {
        global $DBH;
        if (isset($GLOBALS["data"]->Category) && isset($GLOBALS["data"]->Brand) && isset($GLOBALS["data"]->Reference) && isset($GLOBALS["data"]->Description) && isset($GLOBALS["data"]->Unit) && isset($GLOBALS["data"]->Price) && isset($GLOBALS["data"]->Stock) && isset($GLOBALS["data"]->LowStock) && isset($GLOBALS["data"]->PackedPer) && isset($GLOBALS["data"]->Provider) && isset($GLOBALS["data"]->TID) && isset($GLOBALS["data"]->Comment) && isset($GLOBALS["data"]->ShowPublic)) {
            try {
                $STH = $DBH->prepare("INSERT INTO " . TableService::getTable(TableEnum::ITEMS) . " (Category, Brand, Reference, Description, Unit, Price, Stock, LowStock, PackedPer, Provider, TID, Comment, Public) VALUES (:Category, :Brand, :Reference, :Description, :Unit, :Price, :Stock, :LowStock, :PackedPer, :Provider, :TID, :Comment, :Public)");
                $STH->bindParam(":Category", $GLOBALS["data"]->Category);
                $STH->bindParam(":Brand", $GLOBALS["data"]->Brand);
                $STH->bindParam(":Reference", $GLOBALS["data"]->Reference);
				$STH->bindParam(":Description", $GLOBALS["data"]->Description);
                $STH->bindParam(":Unit", $GLOBALS["data"]->Unit);
                $STH->bindParam(":Price", $GLOBALS["data"]->Price);
				$STH->bindParam(":Stock", $GLOBALS["data"]->Stock);
                $STH->bindParam(":LowStock", $GLOBALS["data"]->LowStock);
                $STH->bindParam(":PackedPer", $GLOBALS["data"]->PackedPer);
				$STH->bindParam(":Provider", $GLOBALS["data"]->Provider);
				$STH->bindParam(":TID", $GLOBALS["data"]->TID);
				$STH->bindParam(":Comment", $GLOBALS["data"]->Comment);
				$STH->bindParam(":Public", $GLOBALS["data"]->ShowPublic);
                $STH->execute();
            } catch (Exception $e) {
                $GLOBALS["error"] = "Er is iets fout gelopen...";
                $app->error();
            }
        } else {
            $GLOBALS["error"] = "Onvoldoende parameters meegegeven!";
            $app->error();
        }
    });

    /**
     * POST naar /api/items/:itemId
     * Hier kan je een bepaald id aanpassen. Als we specifiek 1 item zouden
     * moeten kunnen opvragen, zouden we ook een GET naar /api/items/:itemId
     * kunnen maken.
     **/
    $app->post('/:id', function($id) use ($app) {
        generateResponse(ItemService::updateItem($id, $GLOBALS["data"]));
    });
	
	 /**
     * POST naar /api/items/delete/:itemId
     * Hier kan je een bepaald id verwijderen
     **/
	$app->post('/delete/:id', function($id) use ($app) {
        generateResponse(ItemService::deleteItem($id));
    });
	


});

$app->group('/finances', function() use ($app) {

    /**
     * GET naar /api/finances
     * Dit geeft een lijst van alle finances terug
     **/
    $app->get('/', function() use ($app) {
        echo json_encode(FinancesService::getFinances($app->request()->get('fromDate'), $app->request()->get('toDate')));
    });

    /**
     * POST naar /api/finances
     * POST, dus data van client naar server. We gaan een nieuw transactions opslaan
     **/
    $app->post('/', function() use ($app) {
        generateResponse(FinancesService::createTransaction($GLOBALS["data"]));
    });

});

$app->post('/checkout', function() use ($app) {
    global $DBH;
    if (!is_array($GLOBALS["data"])) {
        // We verwachten hier een array van items! We kunnen dit checken, maar moet natuurlijk niet
        $GLOBALS["error"] = "Data is not an array";
        $app->error();
    } else {
        try {
            $DBH->beginTransaction();
            foreach ($GLOBALS["data"] as $item) {
                $STH = $DBH->prepare("UPDATE " . TableService::getTable(TableEnum::ITEMS) . " SET Stock = Stock - :amount WHERE id=:id");
                $STH->bindParam(":amount", $item->amount);
                $STH->bindParam(":id", $item->id);
                $STH->execute();

                $financeInsert = FinancesService::createTransaction($item);
                if ($financeInsert["status"] == -1) {
                    throw new Exception($financeInsert["error"]);
                }
            }
            $DBH->commit();
        } catch (Exception $e) {
            $DBH->rollBack();
            $GLOBALS["error"] = $e->getMessage();
            $app->error();
        }
    }
    echo json_encode(null);
});

$app->group('/export', function() use ($app) {

    $app->get('/items', function() use ($app) {
		try {
			global $DBH;
			header("Content-Type: text/csv");
			header("Content-Disposition: attachment; filename=Tournevie_items.csv");
			//Disable caching
			header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
			header("Pragma: no-cache"); // HTTP 1.0
			header("Expires: 0"); // proxies
			$output = fopen("php://output","w");
			$STH = $DBH->query("SELECT * FROM " . TableService::getTable(TableEnum::ITEMS));
			while($row = $STH->fetch(PDO::FETCH_ASSOC)) {
				fputcsv($output, $row);
			}
			fclose($output);
		} catch (Exception $e) {
			$GLOBALS["error"] = "Er is iets fout gelopen...";
			$app->error();
		}
    });

	$app->get('/finances', function() use ($app) {
		try {
			header("Content-Type: text/csv");
			header("Content-Disposition: attachment; filename=Tournevie_finances.csv");
			//Disable caching
			header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
			header("Pragma: no-cache"); // HTTP 1.0
			header("Expires: 0"); // proxies
			$output = fopen("php://output","w");
            $STH = FinancesService::getFinancesRaw($app->request()->get('fromDate'), $app->request()->get('toDate'));
			while($row = $STH->fetch(PDO::FETCH_ASSOC)) {
				fputcsv($output, $row);
			}
			fclose($output);
		} catch (Exception $e) {
			$GLOBALS["error"] = "Er is iets fout gelopen...";
			$app->error();
		}
    });
});

$app->group('/admin', function() use ($app) {

    $app->post('/syncTestEnvironment', function () use ($app) {
        try {
            TableService::syncTestEnvironment();
            echo json_encode(null);
        } catch (Exception $e) {
            $GLOBALS["error"] = "Er is iets fout gelopen...";
            $app->error();
        }
    });

});

function generateResponse($response) {
    global $app;
    if ($response["status"] == 0) {
        if (isset($response["data"]))
            echo json_encode($response["data"]);
        echo json_encode(null);
    } else {
        if (!isset($response["error"])) {
            $response["error"] = "Er is iets fout gelopen...";
        }
        $GLOBALS["error"] = $response["error"];
        $app->error();
    }
}

$app->run();

exit();
