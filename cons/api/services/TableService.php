<?php

require_once(__DIR__ . "/TableEnum.php");
require_once(__DIR__ . "/../pdoconnect.php");

class TableService {

    /**
     * Get the correct table name - if test, prepend 't_' to it
     **/
    public static function getTable($tableName) {
        if (!TableEnum::isValidValue($tableName)) {
            throw new Exception("table does not exist");
        }

        if (isset($_SESSION["login"]) && $_SESSION["login"] == "test") {
            return 't_' . $tableName;
        } else {
            return $tableName;
        }
    }

    /**
     * Call to sync the test environment with production settings
     */
    public static function syncTestEnvironment() {
        global $DBH;

        foreach (TableEnum::getConstants() as $table) {
            try {
                $DBH->exec("DROP TABLE t_" . $table);
            } catch (Exception $e) {
                // ignore, perhaps the table wasn't created yet
            }
            $DBH->exec("CREATE TABLE t_" . $table . " AS ( SELECT * FROM " . $table . " )");
        }
    }

}