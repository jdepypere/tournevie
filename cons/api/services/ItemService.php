<?php

require_once(__DIR__ . "/TableService.php");
require_once(__DIR__ . "/TableEnum.php");

class ItemService {

    public static function getItemById($id) {
        global $DBH;
        try {
            $STH = $DBH->prepare("SELECT * FROM " . TableService::getTable(TableEnum::ITEMS) . " WHERE ID=:id ");
    		$STH->bindParam(':id', $id);
            $STH->execute();
            return ["status" => 0, "data" => $STH->fetchAll()];
        } catch (Exception $e) {
            return ["status" => -1, "error" => $e];
        }
    }

	public static function deleteItem($id) {
        global $DBH;
        try {
            $STH = $DBH->prepare("DELETE FROM " . TableService::getTable(TableEnum::ITEMS) . " WHERE ID=:id ");
    		$STH->bindParam(':id', $id);
            $STH->execute();
            return ["status" => 0];
        } catch (Exception $e) {
            return ["status" => -1, "error" => $e];
        }
    }
	
    public static function updateItem($id, $data) {
        global $DBH;
        $allowedCols = [ "Category", "Brand", "Reference", "Description", "Unit", "Price", "Stock", "LowStock", "PackedPer", "Provider", "TID", "Location", "Comment", "Public"];
        if (isset($data->name) && isset($data->value)) {
            if (in_array($data->name, $allowedCols)) {
				if ($data->name == "Public") {
                    if (empty($data->value)) {
                        $data->value = "0";
                    } else {
                        $data->value = "1";
                    }
                }
                try {
                    $STH = $DBH->prepare("UPDATE " . TableService::getTable(TableEnum::ITEMS) . " SET " . $data->name . " = :value WHERE id=:id");
                    $STH->bindParam(':value', $data->value);
                    $STH->bindParam(':id', $id);
                    $STH->execute();
                    return ["status" => 0];
                } catch (Exception $e) {
                    return ["status" => -1, "error" => "Er is iets fout gelopen..."];
                }
            } else {
                return ["status" => -1, "error" => "Databank kolom niet toegestaan."];
            }
        } else {
            return ["status" => -1, "error" => "Databank kolom niet toegestaan."];
        }
    }

}

?>
