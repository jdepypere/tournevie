<?php

require_once(__DIR__ . "/TableService.php");
require_once(__DIR__ . "/TableEnum.php");

class FinancesService
{

    public static function createTransaction($data)
    {
        global $DBH;
        if (isset($data->Datetime) && isset($data->Category) && isset($data->Brand) && isset($data->Reference) && isset($data->Description) && isset($data->Number) && isset($data->Comment) && isset($data->Income)) {
            try {
                $STH = $DBH->prepare("INSERT INTO " . TableService::getTable(TableEnum::FINANCES) . " (Date, Category, Brand, Reference, Description, Number, Comment, Income) VALUES (:Date, :Category, :Brand, :Reference, :Description, :Number, :Comment, :Income)");
                $STH->bindParam(":Date", $data->Datetime);
                $STH->bindParam(":Category", $data->Category);
                $STH->bindParam(":Brand", $data->Brand);
                $STH->bindParam(":Reference", $data->Reference);
                $STH->bindParam(":Description", $data->Description);
                $STH->bindParam(":Number", $data->Number);
                $STH->bindParam(":Comment", $data->Comment);
                $STH->bindParam(":Income", $data->Income);
                $STH->execute();
            } catch (Exception $e) {
                return ["status" => -1, "error" => $e];
            }
        } else {
            return ["status" => -1, "error" => "Onvoldoende parameters meegegeven!"];
        }
        return ["status" => 0];
    }

    /**
     * @param $fromTimestamp Unix timestamp in seconds from which the finances should be given. If unset 0 will be taken
     * @param $toTimestamp Unix timestamp in seconds up to which the finances should be given. If unset, PHP_INT_MAX
     * will be taken. Take care, this only goes up to the year 2038! https://en.wikipedia.org/wiki/Year_2038_problem
     * @return array all finances
     */
    public static function getFinances($fromTimestamp, $toTimestamp)
    {
        $STH = self::getFinancesRaw($fromTimestamp, $toTimestamp);
        return $STH->fetchAll();
    }

    public static function getFinancesRaw($fromTimestamp, $toTimestamp) {
        global $DBH;
        if ($fromTimestamp == null)
            $fromTimestamp = 0;

        if ($toTimestamp == null)
            $toTimestamp = PHP_INT_MAX;

        $STH = $DBH->prepare("SELECT * FROM " . TableService::getTable(TableEnum::FINANCES) . " WHERE Date >= :fromDate AND Date < :toDate");
        $STH->bindValue(":fromDate", date("Y-m-d H:i:s", $fromTimestamp));
        $STH->bindValue(":toDate", date("Y-m-d H:i:s", $toTimestamp));
        $STH->execute();

        return $STH;
    }
}

