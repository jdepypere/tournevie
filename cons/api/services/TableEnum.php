<?php

require_once(__DIR__ . "/BasicEnum.php");

abstract class TableEnum extends BasicEnum {

    const FINANCES = 'finances';
    const ITEMS = 'items';

}