var content_cat = '';
var content_type = '';
var content_type_arr = [];

var template_checkout_row;
var template_content_type_option;
var template_editable_type_number_float;
var template_editable_type_checklist;

var template_editable_type_text;

$(document).ready(function () {
    toastr.options.closeButton = true;
    toastr.options.progressBar = true;

    compileHandlebarTemplates();

    reloadItems();

    /**
     * Small DataTable fix for finding cols, is going to be added in the next version
     */
    $.fn.dataTable.Api.register('column().getName()', function () {
        var ctx = this.context[0];
        return this.length ?
            ctx.aoColumns[this[0][0]].sName :
            null;
    });

	/* Custom filtering function for datatablesr items-table with lowstockcheckbox */
	$.fn.dataTable.ext.search.push(
		function( settings, data ) {
			/* for items-table with lowstockcheckbox */
			if (settings.nTable.id == 'items_table') {
				if ($('#managefilterlowstock').is(':checked')){
					var stockalert = parseFloat( data[7] ) || 0;
					if (stockalert < 0) {
						return false;
					} else {
						var stock = parseFloat( data[6] ) || 0;					
						return stock <= stockalert;
					}
				} else {
					return true;
				}
			}
			/* for finance-table with datepicker */
			if (settings.nTable.id == 'finance_table') {
				var datetime = moment(data[0]).format('YYYY-MM-DD');
                var financeRange = $('#financerange');
				var startdate = financeRange.data('daterangepicker').startDate.format('YYYY-MM-DD');
				var enddate = financeRange.data('daterangepicker').endDate.format('YYYY-MM-DD');
				return (moment(datetime).isSameOrAfter(startdate) && moment(datetime).isSameOrBefore(enddate));
			}
			return true;
		}
	);
	
    $('#price_table').DataTable({
        paging: true,
		pageLength: 25,
        columns: [
            {data: 'Category'},
            {data: 'Brand'},
            {data: 'Reference'},
            {data: 'Description'},
            {
                data: {
                    price: 'Price',
                    unit: 'Unit'
                },
                render: function (data, type) {
					if (type === 'display' || type === 'filter') {
						return parseFloat(data.Price).toFixed(2) + "&nbsp;&euro; / " + data.Unit;
					}
					return data.Price;
                }
            },
            {data: 'Stock'}
        ]
    });
	
	var financetable = $('#finance_table').DataTable({
        paging: true,
		pageLength: 25,
		dom: 'l<"financerangediv">frtip',
        columns: [
			{data: 'Date'},
            {data: 'Category'},
            {data: 'Brand'},
            {data: 'Reference'},
            {data: 'Description'},
			{data: 'Number'},
			{data: 'Comment'},
			{data: 'Income'}
        ]
    });

    var itemstable = $('#items_table').DataTable({
        paging: true,
		pageLength: 25,
		dom: 'l<"lowstockcheckbox">frtip',
        columns: [
            {
                data: null,
                name: 'Category',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'Category', value: data.Category});
					}
					return data.Category;
                }
            },
            {
                data: null,
                name: 'Brand',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'Brand', value: data.Brand});
					}
					return data.Brand;
                }
            },
            {
                data: null,
                name: 'Reference',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'Reference', value: data.Reference});
					}
					return data.Reference;
                }
            },
            {
                data: null,
                name: 'Description',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'Description', value: data.Description});
					}
					return data.Description;
                }
            },
            {
                data: null,
                name: 'Unit',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'Unit', value: data.Unit});
					}
					return data.Unit;
                }
            },
            {
                data: null,
                name: 'Price',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_number_float({
							id: data.ID,
							name: 'Price',
							value: data.Price,
							extra: 'data-step="0.01"'
						});
					}
					return data.Price;
                }
            },
            {
                data: null,
                name: 'Stock',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_number_float({id: data.ID, name: 'Stock', value: data.Stock});
					}
					return data.Stock;
                }
            },
            {
                data: null,
                name: 'LowStock',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_number_float({id: data.ID, name: 'LowStock', value: data.LowStock});
					}
					return data.LowStock;
                }
            },
            {
                data: null,
                name: 'PackedPer',
                render: function (data, type) {
                    if (type === 'display') {
						return template_editable_type_number_float({id: data.ID, name: 'PackedPer', value: data.PackedPer});
					}
					return data.PackedPer;
                }
            },
            {
                data: null,
                name: 'Provider',
                render: function (data, type) {
                    if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'Provider', value: data.Provider});
					}
					return data.Provider;
                }
            },
            {
                data: null,
                name: 'TID',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'TID', value: data.TID});
					}
					return data.TID;
                }
            },
            {
                data: null,
                name: 'Location',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'Location', value: data.Location});
					}
					return data.Location;
                }
            },
            {
                data: null,
                name: 'Comment',
                render: function (data, type) {
					if (type === 'display') {
						return template_editable_type_text({id: data.ID, name: 'Comment', value: data.Comment});
					}
					return data.Comment;
                }
            },
            {
                data: null,
                name: 'Public',
                orderable: false,
                render: function (data) {
                    return template_editable_type_checklist({id: data.ID, name: 'Public', value: data.Public});
                }
            },
            {
                data: null,
                name: 'Delete',
                orderable: false,
                render: function () {
                    return '<button type="button" class="btn btn-default delete_item"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>';
                }
            }
        ]
    }).on('draw.dt', function() {
        parseEditableFields();
    });
	
	$('.lowstockcheckbox').on('change', function() {
        itemstable.draw();
    });
	
	$("div.lowstockcheckbox").html('<input type="checkbox" id="managefilterlowstock">Show only low stock');
	
	$("div.financerangediv").html('<label for="financerange" class="control-label" style="padding: 2px 5px;">Range:</label><div id="financerange" class="pull-right" style="background: #fff; cursor: pointer; padding: 2px 10px; border: 1px solid #ccc;"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;<span></span> <b class="caret"></b></div>');
	
    $(document).on('click', '.delete_checkout', function () {
        $(this).closest('tr').remove();
        updateTotalCheckoutPrice();
    });

    $(document).on('click', '.delete_item', function () {
        // determine which item to delete
        var row = $(this).closest('tr');
        var id = row.data('id');
        console.log("deleting item with id " + id);
        // call to api to drop item
        if (confirm('Delete item?')) {
            $.ajax({
                type: 'POST',
                url: 'api/items/delete/' + id,
                success: function () {
                    // reload item table
                    reloadItems();
                },
                error: function () {
                    console.error();
                }
            });
        }
    });

    $('#manageColsModal').find('input:checkbox').change(function() {
        hideColumn(this.value, this.checked);
    });

    $("#item-form").submit(function (e) {
        $.ajax({
            type: 'POST',
            url: 'api/items',
            data: JSON.stringify({
                'Category': $('#item-Category').val(),
                'Brand': $('#item-Brand').val(),
                'Reference': $('#item-Reference').val(),
                'Description': $('#item-Description').val(),
                'Unit': $('#item-Unit').val(),
                'Price': $('#item-Price').val(),
                'Stock': $('#item-Stock').val(),
                'LowStock': $('#item-LowStock').val(),
                'PackedPer': $('#item-PackedPer').val(),
                'Provider': $('#item-Provider').val(),
                'TID': $('#item-TID').val(),
                'Comment': $('#item-Comment').val(),
                'ShowPublic': $('#item-Public').val()
            }),
            contentType: "application/json",
            dataType: "text",
            success: function () {
                // hide and reset modal
                $('#addItemModal').modal('hide');
                setDefaultItemModal();
                // reload item table
                reloadItems();
            },
            error: function (data) {
                console.error(data);
            }
        });
        e.preventDefault();
    });

    $("#fin-form").submit(function (e) {
        // post to values to the finances table
        $.ajax({
            type: 'POST',
            url: 'api/finances',
            data: JSON.stringify({
                'Datetime': myGetDatetime(),
                'Category': $('#fin-Category').val(),
                'Brand': $('#fin-Brand').val(),
                'Reference': $('#fin-Reference').val(),
                'Description': $('#fin-Description').val(),
                'Number': $('#fin-Number').val(),
                'Comment': $('#fin-Comment').val(),
                'Income': $('#fin-Income').val()
            }),
            contentType: "application/json",
            dataType: "text",
            success: function () {
                console.log("finance form done");
                // hide and reset modal
                $('#addFinanceModal').modal('hide');
                setDefaultFinanceModal();
            },
            error: function (data) {
                console.error(data);
            }
        });
        e.preventDefault();
    });

    $("#submitAddItemForm").click(function () {
        $("#item-form").submit();
    });

    $("#submitAddFinanceForm").click(function () {
        $("#fin-form").submit();
    });

    $("#checkout_table_tbody").on('change', 'input', function (e) {
        e.stopPropagation();
        var row = $(this).closest('tr');
        updateCheckoutIncome(row);
    }).on('select2:select', '.wrapcat', function() {
		var newcategory = $(this).find('option:selected').text();
		updateTypeSelect($(this),newcategory,'place');	
		// first argument should be type select box, but for mode 'unselect' it is not used
		// so for simplicity we give catselectbox
		updateCheckoutRowPrice($(this),'unselect');		
		disableCheckoutRow($(this).closest('tr'), true);
	}).on('select2:unselect', '.wrapcat', function() {
		updateTypeSelect($(this),"",'place');
		// first argument should be type select box, but for mode 'unselect' it is not used
		// so for simplicity we give catselectbox
		updateCheckoutRowPrice($(this),'unselect');
		disableCheckoutRow($(this).closest('tr'), true);
	}).on('select2:select', '.wraptype', function() {
		// if no category is set, set the category
		var row = $(this).closest('tr');
		var catselectbox = row.find('.wrapcat');
		var currentCategory = catselectbox.find('option:selected').text();
		// if no category is set, set it
		if (currentCategory == "") {
			var typeselectedvalue = $(this).find('option:selected').data('sqlid');
			desiredCategory = $(this).find('option:selected').data('category');
			catselectbox.prop("value",desiredCategory);
			// we can only update selectbox with calling change
			// because we don't want to trigger any other actions
			// we use eventlisteners on the select and unselect options
			catselectbox.change();
			updateTypeSelect(catselectbox,desiredCategory,typeselectedvalue);
		}
		updateCheckoutRowPrice($(this),'select');
		disableCheckoutRow($(this).closest('tr'), false);
	}).on('select2:unselect', '.wraptype', function() {
		updateCheckoutRowPrice($(this),'unselect');
		disableCheckoutRow($(this).closest('tr'), true);
	}).arrive('select', function() {
		$(this).select2({
			placeholder: "Type to search",
			allowClear: true,
			//allows wider dropdown when active to show more text
			dropdownAutoWidth: true
		});
	});
	
	updateHiddenColsCheckboxes();
	
	// function that gets called when finance data range changes
	function finrangechanged(start, end) {
		$('#financerange').find('span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		financetable.draw();
	}
	
	// initiliase finance data range picker
	var initstart = moment().subtract(29, 'days');
	var initend = moment();
	$('#financerange').daterangepicker({
		locale: {format: 'DD-MM-YYYY'},
		minDate: moment("01-01-2016", "MM-DD-YYYY"),
		startDate: initstart,
		endDate: initend,
		alwaysShowCalendars: true,
		ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
		   'All Time': [moment("01-01-2016", "MM-DD-YYYY"), moment()]
        }
	}, finrangechanged);
	finrangechanged(initstart, initend);

    environmentSync.init();
	
});

/**
* in the checkouttab, update the options in type selectbox when the category selectbox changed
* unfortunately hide() and show() does not work with select2
* prop("disabled", true) combined with css display:none only works the first time
* instead we will remove all options and only append valid ones
**/
function updateTypeSelect(selectboxcat, newcategory, typevalue) {
	var row = selectboxcat.closest('tr');
	var selectboxtype = row.find('.wraptype');
	selectboxtype.empty();
	selectboxtype.append("<option data-category='placeholder'></option>");
	// in itemcategory, & is shown as &amp; adapt newcategory
	newcategory = newcategory.replace(/\&/g,'&amp;');
	$(content_type_arr).each( function (index, item) {
		var itemcategory = item.match('data-category="(.*)"')[1];
		if (itemcategory == newcategory || newcategory == "") {
				selectboxtype.append(item);
		}
	});
	if (typevalue != 'place'){
		selectboxtype.prop("value",typevalue);
	}
	selectboxtype.change();
}



/**
 * Op te roepen wanneer de tabel met waardes moet herladen worden
 **/
function addCheckoutLine() {
    //load entire category
    $('#checkout_table_tbody').append(template_checkout_row({category: content_cat, type: content_type}));
}

/**
 * Laad de price en items tabellen want dit is dezelfde query naar de api
 **/
function reloadItems() {
    $.ajax({
        url: 'api/items',
        success: function (result) {
            // Update #price_table
            var priceDataTable = $('#price_table').dataTable().api();
            priceDataTable.clear();
            priceDataTable.rows.add(result);
            priceDataTable.draw();

            var itemsDataTable = $('#items_table').dataTable().api();
            itemsDataTable.clear();
            itemsDataTable.rows.add(result);
            itemsDataTable.draw();
			
			reloadLocalItems(result);

        }
    });
}

function reloadLocalItems(result) {
	content_cat = '';
	content_type = '';
	content_type_arr = [];

	var categories = [];
	for (var i = 0, len = result.length; i < len; i++) {
		if ($.inArray(result[i].Category, categories) == -1) {
			categories.push(result[i].Category);
			content_cat += '<option>' + result[i].Category + '</option>';
		}
		content_type += template_content_type_option(result[i]);
		content_type_arr.push(template_content_type_option(result[i]));
	}
	
	reloadCheckout();		
}

/**
 * Reloads only prices table
 **/
function reloadPrices() {
    $.ajax({
        url: 'api/items',
        success: function (result) {
            var priceDataTable = $('#price_table').dataTable().api();
            priceDataTable.clear();
            priceDataTable.rows.add(result);
            priceDataTable.draw();
			
			reloadLocalItems(result);
        }
    });
}

/**
 * Op te roepen wanneer de tabel met finances moet herladen worden
 **/
function reloadFinances() {
    /**
     * Door de eerste drie lijnen code hieronder doen we een ajax call
     * naar onze REST api onder api/finances. Success wordt opgeroepen
     * wanneer we een HTTP status 200 terug krijgen. Hier wordt
     * van een ideale wereld uitgegaan en worden er geen errors getoond.
     **/
    $.ajax({
        url: 'api/finances',
		//data: {
		//	'fromDate': 0,
		//	'toDate': moment().unix()
		//},
        success: function (result) {
			var financeDataTable = $('#finance_table').dataTable().api();
            financeDataTable.clear();
            financeDataTable.rows.add(result);
            financeDataTable.draw();
			
            var totalIncome = 0.0;
            for (var i = 0, len = result.length; i < len; i++) {
                totalIncome += parseFloat(result[i].Income);
            }
            document.getElementById("fin_totalIncome").innerHTML = parseFloat(totalIncome).toFixed(2) + "&nbsp;&euro;";
        }
    });
}

/**
 * Reset de checkout tafel
 **/
function reloadCheckout() {
    $("#checkout_table_tbody").empty();
    addCheckoutLine();
    updateTotalCheckoutPrice();
}

/**
 * Compute the total checkout price
 **/
function updateTotalCheckoutPrice() {
    //make sum a float?
    var sum = 0;
    $('.checkoutincome').each(function (index, object) {
        sum += parseFloat($(object).text());
    });
    document.getElementById("checkout_Total").innerHTML = parseFloat(sum).toFixed(2);
}

function updateCheckoutRowPrice(typeselectbox, mode) {
	var row = typeselectbox.closest('tr');
	if (mode == 'select'){
		var selectedOption = typeselectbox.find('option:selected');
		row.find('.checkoutprice')[0].innerHTML = parseFloat(selectedOption.data('price')).toFixed(2);
		row.find('.checkoutunit')[0].innerHTML = selectedOption.data('unit');
	}
	else {
		row.find('.checkoutprice')[0].innerHTML = parseFloat("0.00").toFixed(2);
		row.find('.checkoutunit')[0].innerHTML = "";	
	}
    updateCheckoutIncome(row);
}

function updateCheckoutIncome(row) {
    var price = parseFloat(row.find('.checkoutprice')[0].innerHTML);
    var adjustment = parseFloat(row.find(".checkoutadjustment input")[0].value);
    var amount = parseFloat(row.find(".checkoutnumber input")[0].value);
    row.find('.checkoutincome')[0].innerHTML = parseFloat(( price * amount ) + adjustment).toFixed(2);
    updateTotalCheckoutPrice();
}

function disableCheckoutRow(row, disable) {
	if (disable) {
		row.find('.checkoutnumber input').prop("value", 1);
		row.find('.checkoutcomment input').prop("value", "");
		row.find('.checkoutadjustment input').prop("value", 0);
	}
    row.find('.checkoutnumber input').prop("disabled", disable);
    row.find('.checkoutcomment input').prop("disabled", disable);
    row.find('.checkoutadjustment input').prop("disabled", disable);
}

function setDefaultFinanceModal() {
    document.getElementById('fin-Category').value = "";
    document.getElementById('fin-Brand').value = "";
    document.getElementById('fin-Reference').value = "";
    document.getElementById('fin-Description').value = "";
    document.getElementById('fin-Number').value = 1;
    document.getElementById('fin-Comment').value = "";
    document.getElementById('fin-Income').value = 0;
}

function setDefaultItemModal() {
    document.getElementById('item-Category').value = "";
    document.getElementById('item-Brand').value = "";
    document.getElementById('item-Reference').value = "";
    document.getElementById('item-Description').value = "";
    document.getElementById('item-Unit').value = "piece";
    document.getElementById('item-Price').value = 0;
    document.getElementById('item-Stock').value = 0;
    document.getElementById('item-LowStock').value = 0;
    document.getElementById('item-PackedPer').value = 0;
    document.getElementById('item-Provider').value = "";
    document.getElementById('item-TID').value = "";
    document.getElementById('item-Comment').value = "";
    document.getElementById('item-Public').value = 1;
}

routie({
    '': function () {
        setPageActive('home');
    },
    'home': function () {
        setPageActive('home');
    },
    'price': function () {
        setPageActive('price');
    },
    'counter': function () {
        setPageActive('counter');
    },
    'stock': function () {
        setPageActive('stock');
    },
    'finance': function () {
        setPageActive('finance');
        reloadFinances();
    },
    'admin': function() {
        setPageActive('admin');
    },
    'logout': function () {
        setPageActive('logout');
    }
});

function setPageActive(page) {
    var main_nav = $('#main-nav');
    main_nav.find('li.active').removeClass('active');
    main_nav.find('a[href$="' + page + '"]').parent().addClass('active');
    $('.content_section.active').removeClass('active');
    $('#content_' + page).addClass('active');
}

/**
 * Checkout. Update amount of stock 
 * Finances update when clicking on the tab
 **/
function performCheckout() {
    var checkoutButton = $('#checkoutButton');
    checkoutButton.button('loading');
    var sendData = [];
	var sendStockAlert = [];
    $("#checkout_table_tbody").find('tr').each(function () {
        var $this = $(this);
        var ctype = $this.find('.checkouttype option:selected');
		if (ctype.data('category') != 'placeholder') {
			var id = ctype.data('sqlid'); // SQL-ID
			var amount = parseFloat($this.find(".checkoutnumber input")[0].value);
			var stock = parseFloat(ctype.data('stock')) || 0;
			var stockalert = parseFloat(ctype.data('stockalert')) || 0;
			if ((stock > stockalert) && (stock-amount <= stockalert) && (stockalert >= 0)){
				sendStockAlert.push({
					'Category': ctype.data('category'),
					'Brand': ctype.data('brand'),
					'Reference': ctype.data('reference'),
					'Description': ctype.data('description'),
					'TID': ctype.data('tid'),
					'NewStock': stock-amount,
					'StockAlert': stockalert
				});
			}
			sendData.push({
				"id": id,
				"amount": amount,
				'Datetime': myGetDatetime(),
				'Category': ctype.data('category'),
				'Brand': ctype.data('brand'),
				'Reference': ctype.data('reference'),
				'Description': ctype.data('description'),
				'Number': amount,
				'Comment': $this.find(".checkoutcomment input")[0].value,
				'Income': parseFloat($this.find('.checkoutincome')[0].innerHTML)
			});
		}
    });
	if (sendData.length == 0){
		toastr.error('No items to check out', 'No checkout performed');
		checkoutButton.button('reset');
	} else {
		$.ajax({
			url: 'api/checkout',
			method: 'POST',
			dataType: 'json',
			data: JSON.stringify(sendData),
			success: function () {
				toastr.success('Checkout Successfull!');
				reloadItems();
			},
			error: function () {
				toastr.error('Try again. If the issue remains, contact the administrator', 'Checkout failed');
			},
			complete: function () {
				console.log('checkout complete');
				mailStockAlert(sendStockAlert);
				checkoutButton.button('reset');
			}
		});
	}
}

function mailStockAlert(lowstock) {
	if(lowstock.length == 0){
		return;
	}
	// create message and header
	var mailsubj= 'Tournevie low stock alert';
	var htmlmsg = '<p>The following Tournevie item has reached low stock:</p>';
	var table = '<table><thead><tr><th>TID</th><th>Category</th><th>Brand</th><th>Reference</th>           <th>Description</th><th>Stock</th><th>StockAlert</th></tr></thead><tbody>';
	$(lowstock).each( function(index, item) {
		table += '<tr><td>' + item.TID + '</td><td>' + item.Category + '</td><td>' + item.Brand + '</td><td>' + item.Reference + '</td><td>' + item.Description + '</td><td>' + item.NewStock + '</td><td>' + item.StockAlert + '</td></tr>';
	});
	table += '</tbody></table>';
	htmlmsg += table + '<p>You can see details of all items low on stock in the <a href="http://cons.tournevie.be/index.php#stock">manage tab</a> (select Show only low stock).</p>';
	// send message	
	$.ajax({
		type: 'POST',
		url: 'lowstockalert.php',
		data: {
			'subject': mailsubj,
			'message': htmlmsg
		},
		success: function (result) {
			console.log(result);
		},
		error: function() {
			toastr.error('Low stock alert not sent', 'No mail sent');
		}
	});
}

function myGetDatetime() {
    return moment().format('YYYY-MM-DD HH:mm:ss');
}

/**
 * Multiline html in js is pas vanaf ES6 beschikbaar, daarom best een framework gebruiken.
 * Hier gaan we alle templates éénmalig compileren.
 */
function compileHandlebarTemplates() {
    template_checkout_row = Handlebars.compile($('#checkout_row').html());
    template_content_type_option = Handlebars.compile($('#content_type_option').html());

    template_editable_type_text = Handlebars.compile($('#editable_type_text').html());
    template_editable_type_number_float = Handlebars.compile($('#editable_type_number_float').html());
    template_editable_type_checklist = Handlebars.compile($('#editable_type_checklist').html());
}

function parseEditableFields() {
    $('.editable').editable({
        type: 'text',
        ajaxOptions: {
            contentType: 'application/json',
            dataType: 'json',
            success: function () {
                //no need to reload items table, only price table
                reloadPrices();
            }
        },
        params: function (params) {
            return JSON.stringify(params);
        }
    });
    $('.editable-float').editable({
        type: 'number',
        ajaxOptions: {
            contentType: 'application/json',
            dataType: 'json',
            success: function () {
				/* while the datatable display is changed by x-editable
				* and the sql is changed with this ajax call
				* we should still edit the value in the datatable to allow searching! */
				//no need to reload items table, only price table
                reloadPrices();
            }
        },
        params: function (params) {
            return JSON.stringify(params);
        }
    });
    $('.editable-checklist').editable({
        type: 'checklist',
        emptytext: 'no',
        source: {
            "1": "yes"
        },
        ajaxOptions: {
            contentType: 'application/json',
            dataType: 'json',
            success: function() {
                reloadPrices();
            }
        },
        params: function (params) {
            return JSON.stringify(params);
        }
    });
}

function hideColumn(col, hidden) {
    hideColWithoutUpdate(col, hidden);
    updateHiddenCols();
}

function hideColWithoutUpdate(col, hidden) {
    $('#items_table').dataTable().api().column(col + ':name').visible(hidden);
}

function updateHiddenCols() {
    console.log('Update hidden cols');
    var api = $('#items_table').dataTable().api();
    var cols = api.columns().visible();
    var hiddenColArray = [];
    for (var i = 0; i < cols.length; i++) {
        if (!cols[i]) { // column is invisible, lets store it
            hiddenColArray.push(api.column(i).getName());
        }
    }
    createCookie('hidden_cols', JSON.stringify(hiddenColArray));
}

function updateHiddenColsCheckboxes() {
    var hiddenColsJSON = readCookie('hidden_cols');
    if (hiddenColsJSON !== null) {
        try {
            var hiddenCols = JSON.parse(hiddenColsJSON);
            console.log('Hidden cols:');
            console.log(hiddenCols);
            for (var i = 0; i < hiddenCols.length; i++) {
                $(':checkbox[value=' + hiddenCols[i] + ']').attr('checked', false);
                hideColWithoutUpdate(hiddenCols[i], false);
            }
        } catch (e) {
            console.error('Could not parse json: ' + e);
        }
    } else {
        console.log('No hidden cols..');
    }
}

/**
 * Object literal test for modularization
 */
var environmentSync = {

    init: function() {
        console.log('init');
        environmentSync.config = {
            button: $('#environmentSyncButton')
        };

        this.setup();
    },

    setup: function() {
        environmentSync.config.button.click(function() {
            environmentSync.executeRequest();
        });
    },

    setLoading: function(loading) {
        environmentSync.config.button.button(loading ? 'loading' : 'reset');
    },

    executeRequest: function() {
        $.ajax({
            url: 'api/admin/syncTestEnvironment',
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                environmentSync.setLoading(true);
            },
            success: function () {
                toastr.success('Sync successfull');
            },
            error: function () {
                toastr.error('Try again. If the issue remains, contact the administrator', 'Sync failed');
            },
            complete: function () {
                environmentSync.setLoading(false);
            }
        });
    }

};

function createCookie(name, value) {
    document.cookie = name + "=" + value + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
