<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.png">

    <title>Tournevie</title>

    <!-- lib css files -->
    <link href="libs/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <link href="libs/toastr/2.1.3/toastr.min.css" rel="stylesheet"/>
    <link href="libs/datatables/1.10.12/datatables.min.css" rel="stylesheet"/>
	<link href="libs/select2/4.0.3/dist/css/select2.css" rel="stylesheet"/>
	<link href="libs/daterangepicker/2.1.25/daterangepicker.css" rel="stylesheet"/>
	
    <link href="css/main.css" rel="stylesheet"/>
    <style type='text/css'>
        .content_section:not(.active) {
            display: none;
        }
    </style>
    <?php if (isset($_SESSION["login"]) && $_SESSION["login"] == 'test') {
        echo '<link rel="stylesheet" href="css/test.css" />';
    } ?>
</head>

<body>

<div class="container">

    <nav class="navbar navbar-default navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <?php if (isset($_SESSION["login"]) && $_SESSION["login"] == 'test') {
                        echo '<div class="test_header">Tournevie Test</div>';
                    } else {
                        echo '<img src="images/Tournevie_white.png" alt="Tournevie">';
                    } ?>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav" id="main-nav">
                    <li class="active"><a href="#home">Home</a></li>
                    <li><a href="#price">Price list</a></li>
                    <li><a href="#counter">Check in/out</a></li>
                    <li><a href="#stock">Manage</a></li>
                    <li><a href="#finance">Finance</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="logout.php">Sign out</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <section id="content_home" class="content_section">
        <p>Welcome to the Tournevie volunteer portal.</p>
        <h4>Useful links</h4>
        <ul>
            <li><a href="http://tournevie-bxl.myturn.com/">Tournevie myturn inventory</a></li>
            <li><a href="https://drive.google.com/drive/folders/0B3btYSP49-K8c08yNUd4dTJYQnc">Attendance calendar</a>
            </li>
            <li><a href="https://titanpad.com/jy3iLOdZ9T">Titanpad</a></li>
            <li>
                <a href="https://docs.google.com/spreadsheets/d/1fiH8Yw159_ybfJFudsxhYX2Juv8OOsqv5dRtxHVTlCA/edit#gid=0">Full
                    inventory (googlesheet)</a></li>
            <li><a href="https://www.dropbox.com/home/Project%20Tournevies">Dropbox</a></li>
        </ul>
		<p>For any problems or suggestions, contact the  <a href="mailto:webmaster@tournevie.be">webmaster</a>.</p>
    </section>

    <section id="content_price" class="content_section">
        <h4>Consumables price list</h4>
        <table id="price_table" class="table table-striped">
            <thead>
            <tr>
                <th>Category</th>
                <th>Brand</th>
                <th>Reference</th>
                <th>Description</th>
                <th>Price</th>
                <th>Stock</th>
            </tr>
            </thead>
        </table>
    </section>

    <section id="content_counter" class="content_section">
        <p>Check in or check out consumables and any other expenses.</p>
        <table class="table table-condensed" id="checkout_table">
            <thead>
            <tr>
                <th>Category</th>
                <th>Type</th>
                <th>Price</th>
                <th>Unit</th>
                <th>Amount</th>
                <th>Comments</th>
                <th>Price Adjustment</th>
                <th>Total Price</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="checkout_table_tbody">
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">Total (&euro;)</td>
                <td id="checkout_Total">0.00</td>
                <td></td>
            </tr>
            </tfoot>
        </table>
        <a onclick="addCheckoutLine()" class="btn btn-primary" target="_blank">Add item</a>
        <button onclick="performCheckout()" id="checkoutButton"
                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Checking out..." class="btn btn-primary" style="float: right;">Checkout
        </button>
    </section>

    <section id="content_stock" class="content_section">
        <p class="bg-danger" style="padding: 10px; margin-top: 5px;">This table allows editing the amount in stock, the
            price and other details of items on sale. Please edit with care!</p>
        <table id="items_table" class="table table-striped">
            <thead>
            <tr>
                <th>Category</th>
                <th>Brand</th>
                <th>Reference</th>
                <th>Description</th>
                <th>Unit</th>
                <th>Price (&euro;)</th>
                <th>Stock</th>
                <th>Stock Alert</th>
                <th>Packed per</th>
                <th>Provider</th>
                <th>TID</th>
                <th>Loc</th>
                <th>Comment</th>
                <th>Public</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="items_table_tbody">
            </tbody>
        </table>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addItemModal">
            Add new item
        </button>
        <a href="api/export/items" class="btn btn-primary" target="_blank">Export list</a>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#manageColsModal">
            Manage columns
        </button>

        <!-- Modal -->
        <div class="modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Item</h4>
                    </div>
                    <div class="modal-body">
                        <form id="item-form">
                            <div class="form-group">
                                <label for="item-Category" class="control-label">Category:</label>
                                <input type="text" class="form-control" id="item-Category" name="Category" required>
                            </div>
                            <div class="form-group">
                                <label for="item-Brand" class="control-label">Brand:</label>
                                <input type="text" class="form-control" id="item-Brand" name="Brand" required/>
                            </div>
                            <div class="form-group">
                                <label for="item-Reference" class="control-label">Reference:</label>
                                <input type="text" class="form-control" id="item-Reference" name="Reference" required/>
                            </div>
                            <div class="form-group">
                                <label for="item-Description" class="control-label">Description:</label>
                                <input type="text" class="form-control" id="item-Description" name="Description"
                                       required/>
                            </div>
                            <div class="form-group">
                                <label for="item-Unit" class="control-label">Unit:</label>
                                <input type="text" class="form-control" id="item-Unit" name="Unit" value="piece"
                                       required/>
                            </div>
                            <div class="form-group">
                                <label for="item-Price" class="control-label">Price:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">&euro;</span>
                                    <input type="number" step="0.5" class="form-control" id="item-Price" value="0"
                                           name="Price" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="item-Stock" class="control-label">Stock:</label>
                                <input type="number" step="1" class="form-control" id="item-Stock" name="Stock"
                                       value="0" required/>
                            </div>
                            <div class="form-group">
                                <label for="item-LowStock" class="control-label">Stock Alert:</label>
                                <input type="number" step="1" class="form-control" id="item-LowStock" name="Low Stock"
                                       value="0" required/>
                            </div>
                            <div class="form-group">
                                <label for="item-PackedPer" class="control-label">Packed Per:</label>
                                <input type="number" step="1" class="form-control" id="item-PackedPer" name="Packed Per"
                                       value="0"/>
                            </div>
                            <div class="form-group">
                                <label for="item-Provider" class="control-label">Provider:</label>
                                <input type="text" class="form-control" id="item-Provider" name="Provider"/>
                            </div>
                            <div class="form-group">
                                <label for="item-TID" class="control-label">Tournevie ID:</label>
                                <input type="text" class="form-control" id="item-TID" name="TID" required/>
                            </div>
                            <div class="form-group">
                                <label for="item-Comment" class="control-label">Comment:</label>
                                <input type="text" class="form-control" id="item-Comment" name="Comment" required/>
                            </div>
                            <div class="form-group">
                                <label for="item-Public" class="control-label">Public: (0 = not shown in price list, 1 =
                                    shown)</label>
                                <input type="number" class="form-control" id="item-Public" value="1" name="Public"
                                       required/>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="submitAddItemForm">Add</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="manageColsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Visible Columns</h4>
                    </div>
                    <div class="modal-body">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Category" checked> Category
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Brand" checked> Brand
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Reference" checked> Reference
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Description" checked> Description
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Unit" checked> Unit
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Price" checked> Price
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Stock" checked> Stock
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="LowStock" checked> Low Stock
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="PackedPer" checked> Packed Per
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Provider" checked> Provider
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="TID" checked> TID
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Location" checked> Location
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Comment" checked> Comment
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="Public" checked> Public
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="content_finance" class="content_section">
        <h4>Financial transactions</h4>
		<form class="form-horizontal" id="counterbalanceform">
			<div class="form-group"> 
				<div class="col-lg-3">
					<label for="fin_totalIncome" class="control-label">Current counter balance:</label>
				</div>	
				<div class="col-lg-2">
					<p class="form-control-static" id="fin_totalIncome" >0.00&euro;</p>
				</div>			
			</div>	
		</form>
		
        <table id="finance_table" class="table table-striped">
            <thead>
            <tr>
                <th>Date time</th>
                <th>Category</th>
                <th>Brand</th>
                <th>Reference</th>
                <th>Description</th>
                <th>Number</th>
                <th>Comment</th>
                <th>Income</th>
            </tr>
            </thead>
            <tbody id="finance_table_tbody">
            </tbody>
        </table>

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addFinanceModal">
            Add transaction
        </button>
		<a href="api/export/finances" class="btn btn-primary" target="_blank">Export all transactions</a>

        <!-- Modal -->
        <div class="modal fade" id="addFinanceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add transaction</h4>
                    </div>
                    <div class="modal-body">
                        <form id="fin-form">
                            <div class="form-group">
                                <label for="fin-Category" class="control-label">Category:</label>
                                <input type="text" class="form-control" id="fin-Category" name="Category" required>
                            </div>
                            <div class="form-group">
                                <label for="fin-Brand" class="control-label">Brand:</label>
                                <input type="text" class="form-control" id="fin-Brand" name="Brand" required/>
                            </div>
                            <div class="form-group">
                                <label for="fin-Reference" class="control-label">Reference:</label>
                                <input type="text" class="form-control" id="fin-Reference" name="Reference" required/>
                            </div>
                            <div class="form-group">
                                <label for="fin-Description" class="control-label">Description:</label>
                                <input type="text" class="form-control" id="fin-Description" name="Description"
                                       required/>
                            </div>
                            <div class="form-group">
                                <label for="fin-Number" class="control-label">Number:</label>
                                <input type="text" class="form-control" id="fin-Number" name="Number" required/>
                            </div>
                            <div class="form-group">
                                <label for="fin-Comment" class="control-label">Comment:</label>
                                <input type="text" class="form-control" id="fin-Comment" name="Comment"/>
                            </div>
                            <div class="form-group">
                                <label for="fin-Income" class="control-label">Income:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">�</span>
                                    <input type="number" step="1" class="form-control" id="fin-Income" name="Income"
                                           required/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="submitAddFinanceForm">Add</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="content_admin" class="content_section">
        <h4>Admin</h4>
        <button id="environmentSyncButton" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Checking out..." class="btn btn-primary">Sync test-environment</button>
    </section>

</div>

<script id="checkout_row" type="text/x-handlebars-template">
    <tr>
        <td data-id="category" class="checkoutcategory">
            <select class="form-control wrapcat" style="width: 175px">
				<option></option>
                {{{category}}}
            </select>
        </td>
        <td data-id="type" class="checkouttype">
            <select id="typeid" class="form-control wraptype" style="width: 400px">
				<option data-category="placeholder"></option>
                {{{type}}}
            </select>
        </td>
        <td class="checkoutprice">
            0.00
        </td>
        <td class="checkoutunit" style="width: 80px">

        </td>
        <td class="checkoutnumber" style="width: 80px">
            <input type="number" step="1" class="form-control checkoutnumberinput" title="Amount" name="Number" value="1" required disabled/>
        </td>
        <td class="checkoutcomment">
            <input type="text" class="form-control" title="Comment" disabled>
        </td>
        <td class="checkoutadjustment" style="width: 80px">
            <input type="number" step="1" class="form-control checkoutadjustmentinput" title="Price Adjustment" name="Adjustment" value="0" required disabled/>
        </td>
        <td class="checkoutincome">
            0.00
        </td>
        <td>
            <button type="button" class="btn btn-default delete_checkout">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </button>
        </td>
    </tr>
</script>

<script id="finance_row" type="text/x-handlebars-template">
    <tr>
        <td>{{Date}}</td>
        <td>{{Category}}</td>
        <td>{{Brand}}</td>
        <td>{{Reference}}</td>
        <td>{{Description}}</td>
        <td>{{Number}}</td>
        <td>{{Comment}}</td>
        <td class="fin_Income">{{Income}} &euro;</td>
    </tr>
</script>

<script id="price_row" type="text/x-handlebars-template">
    <tr>
        <td>{{Category}}</td>
        <td>{{Brand}}</td>
        <td>{{Reference}}</td>
        <td>{{Description}}</td>
        <td>{{Price}} &euro;/{{Unit}}</td>
        <td>{{Stock}}</td>
    </tr>
</script>

<script id="content_type_option" type="text/x-handlebars-template">
    <option value="{{ID}}"
			data-category="{{Category}}"
            data-price="{{Price}}"
            data-brand="{{Brand}}"
            data-sqlid="{{ID}}"
            data-unit="{{Unit}}"
            data-reference="{{Reference}}"
			data-stock="{{Stock}}"
			data-stockalert="{{LowStock}}"
			data-tid="{{TID}}"
            data-description="{{Description}}">{{TID}} - {{Brand}} {{Reference}} {{Description}}</option>
</script>

<script id="editable_type_text" type="text/x-handlebars-template">
    <a href="#" class="editable" data-name="{{name}}" data-pk="{{id}}" data-url="api/items/{{id}}"
       data-title="{{name}}">{{value}}</a>
</script>

<script id="editable_type_number_float" type="text/x-handlebars-template">
    <a href="#" class="editable-float" data-name="{{name}}" data-pk="{{id}}" data-url="api/items/{{id}}"
       data-title="{{name}}" {{{extra}}}>{{prefix}}{{value}}</a>
</script>

<script id="editable_type_checklist" type="text/x-handlebars-template">
    <a href="#" class="editable-checklist" data-name="{{name}}" data-pk="{{id}}" data-url="api/items/{{id}}"
       data-title="{{name}}" data-value="{{value}}"></a>
</script>

<!-- js libs -->
<script src="libs/jquery/3.1.1/jquery.min.js"></script>
<script src="libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="libs/routie/0.3.2/routie.js"></script>
<script src="libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="libs/toastr/2.1.3/toastr.min.js"></script>
<script src="libs/moment.js/2.16.0/moment.min.js"></script>
<script src="libs/handlebars/4.0.5/handlebars.js"></script>
<script src="libs/datatables/1.10.12/datatables.min.js"></script>
<script src="libs/arrive/2.3.1/arrive.min.js"></script>
<script src="libs/select2/4.0.3/dist/js/select2.min.js"></script>
<script src="libs/daterangepicker/2.1.25/daterangepicker.js"></script>


<!-- own js -->
<script src="js/main.js"></script>

</body>
</html>
