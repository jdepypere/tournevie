<?php
if($_POST){
	session_start();
	
	include('vars.php');
	
	if ($sendStockAlert == FALSE) {
		echo "No low stock alerts";
		return;
	}
	
	if ($_SERVER["HTTP_HOST"] == 'localhost' && $sendStockAlertOnLocalhost == FALSE) {
		echo "No low stock alert on localhost";
		return;
	}
		
	$subject = $_POST['subject'];	
	$message = $_POST['message'];

	if (isset($_SESSION["login"])){
		$message .= '<p><br>This alert about the ' . $_SESSION["login"] . ' environment was generated automatically from host ' . $_SERVER["HTTP_HOST"] . '.</p>';	
		if ($_SESSION["login"] == "test") {
			if ($sendStockAlertOnTestEnv == FALSE) {
				echo "No low stock alert on test environment";
				return;
			} else {
				$subject .= ' [TEST]';
			}
		}
	}

	require 'libs/phpmailer/5.2.22/PHPMailerAutoload.php';
	$mail = new PHPMailer;

	//$mail->SMTPDebug = 3;                               // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'send.one.com';  						  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'webmaster@tournevie.be';               // SMTP username
	$mail->Password = 'Tournevie0';                       // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to

	$mail->setFrom('webmaster@tournevie.be', 'Tournevie Webmaster');
	$mail->addAddress($stockAlertRecipient);     // Add a recipient
	$mail->addReplyTo('webmaster@tournevie.be', 'Tournevie Webmaster');

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $subject;
	$mail->Body    = $message;

	if(!$mail->send()) {
		echo 'Low stock alert could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
		echo 'Low stock alert has been sent';
	}
} 
?>