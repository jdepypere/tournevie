/**
 * De code in $(document).ready(function() { ... }) wordt pas uitgevoerd
 * wanneer heel de DOM is ingeladen. DIt is nodig omdat anders mogelijks
 * sommige selectors dat we gebruiken nog niet bestaan.
 **/
$(document).ready(function() {
	reloadPrices();	
});

/**
 * Reloads only prices table
 **/
function reloadPrices() {
	$.ajax({
		url: 'http://cons.tournevie.be/api/items',
		success: function(result) {
			//clear table
			$("#price_table_tbody").empty();
			// loop over all items
			for (var i = 0, len = result.length; i < len; i++) {		
				appendPriceRow(result[i]);
			}
		}
	});
}

function appendPriceRow(result) {
	if (result.Public == 1) {
		$('#price_table_tbody').append(`
			<tr>
				<td>` + result.Category + `</td>
				<td>` + result.Brand + `</td>
				<td>` + result.Reference + `</td>
				<td>` + result.Description + `</td>
				<td>` + result.Price + ` &euro;/` +  result.Unit + `</td>
				<td>` + result.Stock + `</td>
			</tr>
		`);
	}
}	

function myGetDatetime(){
	date = moment().format('YYYY-MM-DD HH:mm:ss');
	return date;
}
